from gelbooru.wrapper import GelbooruRequest

def search(session=None, **kwargs):
    """
    Retrieve raw request data from gelbooru

    Keyword Args:
        session: A session object to use, defaults to None (uses default
            requests session). Can be used to provide caching for the search.
        limit: Number of posts to retrieve. The API imposes a hard limit
            of 100 per request.
        pid: The page number.
        tags: Tags to search for, space seperated. Any tags that work in
            the website work here.
        cid: Change ID of the post, in UNIX time. Requests based on this
            will likely have more than one value.
        id: The post ID. Requests using this should only return one value.
            Therefore, all other arguments are ignored
        md5: The post MD5. Requests using this should only return one value.
            Therefore, all other arguments are ignored

    Returns:
        A GelbooruRequest object containing a list of posts
    """
    return GelbooruRequest(session, **kwargs)
