import xml.etree.ElementTree as etree
import requests
from copy import copy
from dateutil.parser import parse

class GelbooruRequest:
    def __init__(self, session=None, **kwargs):
        """
        Retrieve raw request data from gelbooru

        Keyword Args:
            limit: Number of posts to retrieve. The API imposes a hard limit
                of 100 per request.
            pid: The page number.
            tags: Tags to search for, space seperated. Any tags that work in
                the website work here.
            cid: Change ID of the post, in UNIX time. Requests based on this
                will likely have more than one value.
            id: The post ID. Requests using this should only return one value.
                Therefore, all other arguments are ignored
            md5: The post MD5. Requests using this should only return one value.
                Therefore, all other arguments are ignored

        Returns:
            A GelbooruRequest object containing a list of posts
        """
        possible = set(['limit', 'pid', 'tags', 'cid', 'id', 'md5'])

        if not possible >= set(kwargs.keys()):
            raise BaseException # Invalid parameters

        self._params = kwargs

        if 'md5' in kwargs:
            md5 = kwargs['md5']

            del kwargs['md5']

            if not 'tags' in kwargs:
                kwargs['tags'] = ''

            kwargs['tags'] = kwargs['tags'] + ' md5:{0}'.format(md5)

        if not 'limit' in kwargs:
            self._params['limit'] = 100
        elif kwargs['limit'] > 100:
            raise BaseException # Over request limit
        else:
            self._params['limit'] = kwargs['limit']

        if not 'pid' in kwargs:
            self._params['pid'] = 0
        else:
            self._params['pid'] = kwargs['pid']

        self._session = session if session else requests

        # Compose request URL
        baseurl = 'http://gelbooru.com/index.php?page=dapi&s=post&q=index'
        baseurl = baseurl + '&%s' % ('&'.join(['%s=%s' % e for e in self._params.items()]))

        r = self._session.get(baseurl)

        root = etree.fromstring(r.text)

        self.count = root.attrib['count']
        self.offset = root.attrib['offset']

        self._posts = []
        for post in root:
            self._posts.append(Post(post.attrib, session=self._session))

    def prev_page(self):
        temp = copy(self._params)
        temp['pid'] = temp['pid'] - 1

        if temp['pid'] < 0:
            return None

        req = GelbooruRequest(session=self._session, **temp)

        if req._posts:
            return req
        return None

    def next_page(self):
        temp = copy(self._params)
        temp['pid'] = temp['pid'] + 1

        req = GelbooruRequest(session=self._session, **temp)

        if req._posts:
            return req
        return None

    def __iter__(self):
        return iter(self._posts)

    def __len__(self):
        return len(self._posts)

    def __getitem__(self, key):
        return self._posts[key]


class Post:
    RATINGS = {
        'e': 'explicit',
        's': 'safe',
        'q': 'questionable'
    }

    def __init__(self, dic, session=None):
        self.image = Image(dic['file_url'], dic['width'], dic['height'])
        self.sample = Image(dic['sample_url'], dic['sample_width'], dic['sample_height'])
        self.preview = Image(dic['preview_url'], dic['preview_width'], dic['preview_height'])

        self.rating = Post.RATINGS[dic['rating']]
        self.score = int(dic['score'])
        self.added_at = parse(dic['created_at'])
        self.source = dic['source']

        self.id = int(dic['id'])
        self.change_id = dic['change']
        self.parent = int(dic['parent_id']) if dic['parent_id'] else None
        self.status = dic['status']
        self.md5 = dic['md5']

        self.tags = []
        for tag in dic['tags'].strip().split(' '):
            self.tags.append(Tag(tag, session=session))

class Tag:
    """
    Lazily loading tag class
    """
    TYPES = {
        '0': 'general',
        '1': 'artist',
        '3': 'copyright',
        '4': 'character'
    }

    def __init__(self, name, session=None):
        self._session = session if session else requests
        self.name = name

        self._loaded = False

    @property
    def count(self):
        if not self._loaded:
            self._load()
        return self._count

    @property
    def id(self):
        if not self._loaded:
            self._load()
        return self._id

    @property
    def type(self):
        if not self._loaded:
            self._load()
        return self._type

    @property
    def ambiguous(self):
        if not self._loaded:
            self._load()
        return self._ambiguous

    def _load(self):
        baseurl = 'http://gelbooru.com/index.php?page=dapi&s=tag&q=index'
        baseurl = baseurl + '&name=%s' % self.name
        r = self._session.get(baseurl)

        root = etree.fromstring(r.text)
        for tag in root: # Should only be one, so this is fine
            self._type = Tag.TYPES[tag.attrib['type']]
            self._count = int(tag.attrib['count'])
            self._ambiguous = tag.attrib['ambiguous']
            self._id = int(tag.attrib['id'])


class Image:
    def __init__(self, url, width, height):
        self.url = url
        self.width = int(width)
        self.height = int(height)

    def __repr__(self):
        return '<Image %spx by %spx>' % (width, height)
