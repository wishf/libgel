from setuptools import setup

setup(
    name='libgel',
    version='0.0.1',
    description='Interface to the gelbooru API',
    author='Matthew Summers',
    author_email='matt@wishf.co.uk',

    packages=['gelbooru'],

    install_requires=['requests', 'python-dateutil']
)